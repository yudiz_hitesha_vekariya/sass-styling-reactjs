import React from "react";
import "./Section.scss";

const Section = () => {
  return (
    <div className="hero_container_left">
      <div className="hero_main">
        <div className="hero_container_left1">
          <h1 className="hero_1">They are coming </h1>
          <h2 className="hero_2">Items Recipe</h2>
          <p className="HERO_3">
            food, substance consisting essentially of protein, carbohydrate,
            fat, and other nutrients used in the body of an organism to sustain
            growth and vital processes and to furnish energy. The absorption and
            utilization of food by the body is fundamental to nutrition and is
            facilitated by digestion.
          </p>

          <button type="submit" className="hero_container_btn">
            More info
          </button>
        </div>
      </div>
      <div className="hero_container_right">
        <img src="./h.png" alt="alien" className="hero_container_img"></img>
      </div>
    </div>
  );
};

export default Section;
