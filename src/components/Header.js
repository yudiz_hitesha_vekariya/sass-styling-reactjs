/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import styles from "./Header.module.scss";
import { BiMenuAltRight } from "react-icons/bi";
import Section from "./Section";

const header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.header_content}>
        <div>
          <img src="./2.jpg" className={styles.logo} />
          <span className={styles.logo}>Cooking Time</span>
        </div>
        <div>
          <nav className={styles.nav}>
            <a className={styles.nav__item} href={"/"}>
              Home
            </a>
            <a className={styles.nav__item} href={"/"}>
              Our Recipes
            </a>
            <a className={styles.nav__item} href={"/"}>
              Order Now
            </a>
            <div className={styles.nav__button__container}>
              <Button />
            </div>
          </nav>
        </div>
        <div>
          <div className={styles.header__button__container}>
            <Button />
            <button className={styles.header__toggler}>
              <BiMenuAltRight />
            </button>
          </div>
        </div>
      </div>
      <Section />
    </div>
  );
};

const Button = () => {
  return <button className={styles.button}>Sign up</button>;
};
export default header;
